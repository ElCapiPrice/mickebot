const client = require('../app');

module.exports.Event = class Event {

	constructor() {
		client.on('interactionCreate', async interaction => {
			if (interaction.isCommand()) return this.commands(interaction);
		});
	}

	async commands(interaction) {
		if (interaction.commandName === 'ping') {
			await interaction.reply('Pong!');
		} else if (interaction.commandName === 'server') {
			await interaction.reply(`Server name: ${interaction.guild.name}\nTotal members: ${interaction.guild.memberCount}`);
		} else if (interaction.commandName === 'user') {
			await interaction.reply(`Your tag: ${interaction.user.tag}\nYour id: ${interaction.user.id}`);
		}
	}

};
