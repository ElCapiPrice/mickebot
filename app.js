const { Client, Intents } = require('discord.js');
const client = module.exports = new Client({ intents: [Intents.FLAGS.GUILDS] });
const { FileLoader } = require('./File');
const print = require('./helpers/colorConsole');

const config = require('./config.js');

// Load events
(() => {
	const file = new FileLoader();
	print.log(`Cargando eventos`);
	file.loadEvents();
})();

client.login(config.token);