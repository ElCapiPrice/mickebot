const chalk = require('chalk');

const time = `${new Date().getHours()}:${new Date().getMinutes()}:${new Date().getSeconds()}`;

module.exports = {
    warning: text => {
        console.log(`[WARN] ${time} > ${chalk.yellow(text)}`);
    },
    danger: text => {
        console.log(`[DANGER] ${time} > ${chalk.red(text)}`);
    },
    info: text => {
        console.log(`[INFO] ${time} > ${chalk.blue(text)}`);
    },
    log: text => {
        console.log(`[LOG] ${time} > ${chalk.whiteBright(text)}`);
    }
}