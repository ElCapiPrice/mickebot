const path = require('path');
const fs = require("fs");
const print = require("./helpers/colorConsole");

module.exports.FileLoader = class File {

	constructor() {
		this.absolutePath = path.resolve();
	}

	loadEvents() {
		const eventPath = this.absolutePath + "\\events";
		const eventFiles = fs.readdirSync(eventPath).filter(file => file.endsWith(".event.js"));
		for (const eventFile of eventFiles) {
			try {
				const { Event } = require(`./events/${eventFile}`);
				new Event();
				print.info(`Evento ${eventFile} registrado correctamente`);
			} catch (e) {
				print.danger(`Error al cargar el archivo: ${eventFile}`);
				print.warning(e.stack);
			}
		}
	}

};